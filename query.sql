SELECT --0 as numero,
	catalogo_id, 
	fcreacion, 
	fModificacion, 
	ct.texto, 
	umedida, 
	grcompra, 
	precio, 
	cmat.industria, 
	cmat.rubro, 
	cmat.subrubro, 
	cmat.cod, 
	/*Lista todos los materiales pero si no esta en CatalogoEspecificacionTecnica no le deja usar el tilde para los casos de Acuerdo Marco*/
	isnull ( (Select top 1 matid from CatalogoEspecificacionTecnica CET (nolock) where CA.empid = CET.empid and CA.Matid = CET.Matid ),'N') as descripciontec,
	isnull((Select top 1 Tipo From CatalogoClasMaestro C  (nolock)
		Where c.cia        = 'AR3058018941'
		And   c.Industria  = cmat.industria
		And   c.rubro      = '0'
		And   c.subrubro   = '0'
		And   c.idioma     = 'ES'),'') as tipo,

	isnull(ca.ValorRefInicial,0) as valor, 

	'' as monid,  	 

	'' as empresa,			

	isnull((SELECT convert(varchar(10),(dateadd(d,convert(int,MAX(isnull(leadtime,0)))+convert(int,0),getdate())) ,103) FROM CatalogoMaestro  AS cmLead (nolock) WHERE cmLead.EmpId = cmat.cia 
		And cmLead.Catalogo_id = cmat.Matid),
		(SELECT convert(varchar(10), (dateadd(d,convert(int,MAX(isnull(leadtime,0)))+convert(int,0),getdate())),103) FROM CatalogoClasMaestro (nolock)
			WHERE CatalogoClasMaestro.CIA = cmat.cia 
			AND Rubro = cmat.Rubro
			AND SubRubro = cmat.SubRubro
			AND Idioma = 'ES'
			)
		) as leadsumado,
	convert(varchar(14),getdate(),103) as hoy,

			(Select count (*)  from  catalogomaterialprov  (nolock)
					   Where empid    = 'AR3058018941'
					   And   MatidAdh = catalogo_id 
					   And   Cod      in (Select cod from  CatalogoMaestroDescProv  (nolock)
									 Where empid = 'AR3058018941'
									 And   getdate () < FVigencia 
									 And   getdate () > fCreacion)) as tieneac,

			(Select count (*)  from  catalogomaterialprov CMP (nolock)  Inner Join CatalogoMaterial CMM  (nolock)
					   ON  CMP.Empid      = CMM.Cia 
					   And CMP.Industria  = CMM.Industria 	
					   And CMP.Rubro      = CMM.Rubro
					   And CMP.SubRubro   = CMM.SubRubro
					   Where CMP.empid    = 'AR3058018941'
					   And   isnull(CMP.MatidAdh,'') = ''					   
					   And   CMM.Matid    = catalogo_id
					   And   CMP.Cod      in (Select cod from  CatalogoMaestroDescProv  (nolock)
									 Where empid = 'AR3058018941'
									 And   getdate () < FVigencia 
									 And   getdate () > fCreacion)) as tieneacxgrupo
,(SELECT Top 1 Texto FROM CatalogoClasMaestro AS cl2 (nolock) WHERE cl2.Cia = cmat.cia And cl2.cod = cmat.cod And cl2.industria = cmat.industria AND cl2.Rubro = '0' AND cl2.SubRubro = '0' AND cl2.Idioma = 'ES') AS TextoIndustria
,(SELECT Top 1 Texto FROM CatalogoClasMaestro AS cl2 (nolock) WHERE cl2.Cia = cmat.cia And cl2.cod = cmat.cod And cl2.industria = cmat.industria AND cl2.Rubro = cmat.rubro AND cl2.SubRubro = '0' AND cl2.Idioma = 'ES') AS TextoRubro
,(SELECT Top 1 Texto FROM CatalogoClasMaestro AS cl2 (nolock) WHERE cl2.Cia = cmat.cia And cl2.cod = cmat.cod And cl2.industria = cmat.industria AND cl2.Rubro = cmat.rubro AND cl2.SubRubro = cmat.Subrubro AND cl2.Idioma = 'ES') AS 
TextoSubRubro


,isnull((SELECT replace(Texto,'"','``') FROM catalogotextolargo (nolock) WHERE catalogotextolargo.EmpId = cm.empid AND MatId = cm.catalogo_id),'')AS TextoDetallado
,isnull((SELECT replace(Descripcion,'"','``') FROM  SocioUMedida UM (nolock) WHERE UM.EmpId = cm.empid AND UM.Umedida = cm.umedida),'')AS UnidadMedida
,isnull(ca.Tipo_Mat,'')AS TipoMaterial,isnull((SELECT TOP 1 RTRIM(LTRIM(OrgCompras))  FROM CuGrupoCompras GC (nolock) WHERE EmpId = cm.empid AND RTRIM(LTRIM(Codigo)) = RTRIM(LTRIM(ca.Comprador)) ),'')AS OrgCompraAsignado,
			      isnull(ca.CuentaContable,'') as CuentaContable,
			      isnull((Select Descripcion from cucuentacontable (nolock) where empid = 'AR3058018941' and codigo = ca.CuentaContable),'') as CContableDesc,
			      isnull(ca.Bien_Economico,'') as BienEconomico,
			      isnull(cm.Tipo,'') as ClaseMaterial
			      from catalogomaestro cm  (nolock) inner join catalogotextos ct  (nolock)
			      on  cm.empid = ct.empid 
			      and cm.Catalogo_id = ct.matid	
			      and ct.idioma      = 'ES'
			      left Join CatalogoAtributos ca  (nolock)
			      On  cm.catalogo_id = ca.matid
			      And cm.empid       = ca.empid
         		      left Join CatalogoMaterial cmat  (nolock)
			      ON  cmat.cia        = cm.EmpId
			      And cmat.matid      = cm.catalogo_id
			      left Join CatalogoClasMaestro cl  (nolock)
			      ON  cl.Cia   = cmat.cia
			      And cl.cod     = cmat.cod
			      And cl.industria    = cmat.industria
			      And cl.rubro        = cmat.rubro
			      And cl.subrubro     = cmat.subrubro
			      
			      where cm.empid = ltrim(rtrim('AR3058018941'))  
			      
			       and  cm.catalogo_id = (Select top 1 Matid from catalogomaterial  (nolock) where cia     = cm.empid 
						and   matid   = cm.Catalogo_id
										 and (cod = '1' or cod = '6'))
			      
			      --and   ca.activo = 'S'	
			      --And   cl.idioma = 'ES' And cl.visible ='S'  
   order by catalogo_id 