const express = require('express');
const app = express();
const sql = require("mssql");
const coonfigDB = require('./config')

// Add headers
app.use(function (req, res, next) {
    console.log(req, res)
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


app.get('/', function (req, res) {       
    const query1 = `select top(100)* from usuarios`
    // connect to your database
    sql.close();
    sql.connect(coonfigDB, function (err) {
    
        if (err) console.log(err);

        // create Request object
        var request = new sql.Request();
           
        // query to the database and get the records
        request.query('select top(100) * from usuarios', function (err, recordset) {
            
            if (err) console.log(err)

            // send records as a response
            res.send(recordset);
            
        });
        }
    );
});

const server = app.listen(5000, function () {
    console.log('Server is running..');
});
